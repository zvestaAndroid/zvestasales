package com.zvestasales.activities.signup_flow;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zvestasales.R;
import com.zvestasales.activities.koas_flow.KoasHistoryActivity;
import com.zvestasales.global.BaseActivity;
import com.zvestasales.models.data_model.signupModels.request.SignUpModelRequest;
import com.zvestasales.models.data_model.signupModels.response.SignUpResponse;
import com.zvestasales.models.data_model.userDataModels.UserMainData;
import com.zvestasales.presenters.interfaces.SignupInterface;
import com.zvestasales.presenters.webservices.SignupWebservice;
import com.zvestasales.utils.AppUtils;
import com.zvestasales.utils.activity_handler_utils.RegisterSignUpFlowHandler;
import com.zvestasales.utils.global_data_utils.CurrentStateUtil;
import com.zvestasales.utils.global_data_utils.GlobalDataUtils;
import com.zvestasales.utils.global_data_utils.PreferenceService;
import com.zvestasales.utils.validator_utils.EmailValidator;
import com.zvestasales.utils.validator_utils.PhoneValidator;

public class SignUpActivity extends BaseActivity implements SignupInterface {

    LinearLayout llPassword, llConformPass;
    TextView tvSignUpSubmit, tvTC;
    EditText etFName, etLName, etEmail, etPhone, etPassword, etConformPassword;
    CheckBox cbTC;

    Context _context;
    SignupWebservice signupWebservice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        setData();
        initializeViews();
        onClickView();

        hideSoftKeyboard();
    }

    private void setData() {
        RegisterSignUpFlowHandler.register(this);
        _context = this;
        signupWebservice = new SignupWebservice(this, _context);
    }

    private void initializeViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setToolbar(toolbar, "Sign up", "", 0);
        setToolbar(toolbar, true, -1);
        tvSignUpSubmit = (TextView) findViewById(R.id.tvSignUpSubmit);
        tvTC = (TextView) findViewById(R.id.tvTC);
        etFName = (EditText) findViewById(R.id.etFName);
        etLName = (EditText) findViewById(R.id.etLName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etConformPassword = (EditText) findViewById(R.id.etConformPassword);
        cbTC = (CheckBox) findViewById(R.id.cbTC);
        llPassword = (LinearLayout) findViewById(R.id.llPassword);
        llConformPass = (LinearLayout) findViewById(R.id.llConformPass);

        spannableText();
    }

    private void onClickView() {
        etConformPassword.setOnEditorActionListener(onDonePressListener);
        tvSignUpSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidateUserData())
                    hitSignupService();
            }
        });
    }

    private TextView.OnEditorActionListener onDonePressListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

            // when "correct arrow" is pressed then action_done is event occurs
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                    || (actionId == EditorInfo.IME_ACTION_DONE)) {
                if (isValidateUserData())
                    hitSignupService();
            }
            return true;
        }
    };

    private boolean isValidateUserData() {
        if (AppUtils.isValidText(etFName.getText().toString().trim(), 3, 25)) {
            if (AppUtils.isValidText(etLName.getText().toString().trim(), 3, 25)) {
                if (!etEmail.getText().toString().trim().equals("") && new EmailValidator().validate(etEmail.getText().toString().trim())) {
                    if (AppUtils.isValidText(etPhone.getText().toString().trim(), 10, 10) && new PhoneValidator().validate(etPhone.getText().toString().trim())) {
                        if (AppUtils.isValidText(etPassword.getText().toString().trim(), 6, 0)) {
                            if (etConformPassword.getText().toString().trim().equals(etPassword.getText().toString().trim())) {
                                if (cbTC.isChecked())
                                    return true;
                                else
                                    showToast(getResources().getString(R.string.signupactivity_term_and_condition_valid_text));
                            } else
                                showToast(getResources().getString(R.string.signupactivity_cpassword_valid_text));
                        } else
                            showToast(getResources().getString(R.string.signupactivity_password_valid_text));
                    } else
                        showToast(getResources().getString(R.string.signupactivity_phone_valid_text));
                } else
                    showToast(getResources().getString(R.string.signupactivity_email_valid_text));
            } else
                showToast(getResources().getString(R.string.signupactivity_lname_valid_text));
        } else
            showToast(getResources().getString(R.string.signupactivity_fname_valid_text));
        return false;
    }

    private void hitSignupService() {
        // TODO: hit service for sign up operation.
        if (!showProgressDialog(_context)) {
            return;
        }
        SignUpModelRequest request = new SignUpModelRequest();
        request.first_name = etFName.getText().toString().trim();
        request.last_name = etLName.getText().toString().trim();
        request.email = etEmail.getText().toString().trim();
        request.mobile = etPhone.getText().toString().trim();
        request.password = etPassword.getText().toString().trim();
        request.user_type_id = GlobalDataUtils.USER_TYPE_SELLER;
        request.source_type = GlobalDataUtils.SOURCE_TYPE_DEFAULT;
        signupWebservice.hitSubmitService(request);
    }

    private void performSignup200Ops(SignUpResponse body) {
        PreferenceService.clear();
        UserMainData userMainData = new UserMainData();
        userMainData.userId = body.SUCCESS.UserData.user_id;
        userMainData.fName = body.SUCCESS.UserData.first_name;
        userMainData.lName = body.SUCCESS.UserData.last_name;
        userMainData.email = body.SUCCESS.UserData.email;
        userMainData.mobile = body.SUCCESS.UserData.mobile;
        userMainData.token = AppUtils.generateBearerAuthorigationToken(body.SUCCESS.token);
        userMainData.userTypeId = body.SUCCESS.UserData.user_type_id;
        userMainData.sourceType = GlobalDataUtils.SOURCE_TYPE_DEFAULT;
        CurrentStateUtil.putCurrentUserMainData(userMainData);

        if (body.SUCCESS.UserData.is_verified.equals("0")) {
            startActivity(new Intent(_context, AccountVerificationActivity.class));
        } else {
            showToast("Sign up successfully completed");
            CurrentStateUtil.putLoginState(GlobalDataUtils.LOGIN_STATE);
            Intent intent = new Intent(_context, KoasHistoryActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            RegisterSignUpFlowHandler.removeAll();
        }
    }

    private void spannableText() {
        SpannableString tc = new SpannableString(getResources().getString(R.string.signupactivity_term_and_condition_text));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                goToTermAndConditionPage();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };
        tc.setSpan(clickableSpan, 26, 46, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        tvTC.setText(tc);
        tvTC.setMovementMethod(LinkMovementMethod.getInstance());
        tvTC.setHighlightColor(Color.TRANSPARENT);
    }

    private void goToTermAndConditionPage() {
        // TODO: go to term and condition screen.
        showToast("Coming soon...");
    }

    @Override
    public void submitSignUpResponse(SignUpResponse responseBody) {
        hideProgressDialog();
        performSignup200Ops(responseBody);
    }

    @Override
    protected void onDestroy() {
        clearMemory();
        super.onDestroy();

    }

    private void clearMemory() {
        signupWebservice = null;
        RegisterSignUpFlowHandler.removeLastActivity();
    }
}
