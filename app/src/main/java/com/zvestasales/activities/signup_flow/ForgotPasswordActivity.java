package com.zvestasales.activities.signup_flow;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.zvestasales.R;
import com.zvestasales.global.BaseActivity;
import com.zvestasales.models.data_model.signupModels.request.ForgotPassOtpRequest;
import com.zvestasales.presenters.interfaces.ForgotPasswordInterface;
import com.zvestasales.presenters.webservices.ForgotPasswordWebservice;
import com.zvestasales.utils.global_data_utils.IntentKeys;
import com.zvestasales.utils.global_data_utils.RequestCodes;


public class ForgotPasswordActivity extends BaseActivity implements ForgotPasswordInterface {

    EditText etPhone;
    TextView tvPhoneSend;
    Context _context;
    ForgotPasswordWebservice forgotPasswordWebservice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        setData();
        initializeViews();
        setOnClickView();
    }

    @Override
    protected void onDestroy() {
        forgotPasswordWebservice = null;
        super.onDestroy();
    }

    private void setData(){
        _context = this;
        forgotPasswordWebservice = new ForgotPasswordWebservice(this, _context);
    }

    private void initializeViews(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setToolbar(toolbar, "Forgot Password", "", 0);
        setToolbar(toolbar, true, -1);
        etPhone = (EditText) findViewById(R.id.etPhone);
        tvPhoneSend = (TextView) findViewById(R.id.tvPhoneSend);
    }

    private void setOnClickView(){
        tvPhoneSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etPhone.getText().toString().trim().length() == 10){
                    hitServiceForSendOtp();
                }else
                    showToast("Enter valid phone number.");
            }
        });
    }

    private void hitServiceForSendOtp(){
        if (!showProgressDialog(_context)) {
            return;
        }
        ForgotPassOtpRequest request = new ForgotPassOtpRequest();
        request.mobile = etPhone.getText().toString().trim();
        forgotPasswordWebservice.hitResendOtpService(request);
    }

    @Override
    public void receiveOtp(String successResponseBody) {
        hideProgressDialog();
        showToast("Please check your mobile number for reset password.");
        Intent intent = new Intent(_context, ResetPasswordActivity.class);
        intent.putExtra(IntentKeys.INTENT_KEY_PHONE, etPhone.getText().toString().trim());
        startActivityForResult(intent, RequestCodes.FORGOT_PASSWORD_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RequestCodes.FORGOT_PASSWORD_REQUEST_CODE && resultCode == RESULT_OK){
            finish();
        }
    }
}
