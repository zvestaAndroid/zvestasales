package com.zvestasales.activities.signup_flow;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.zvestasales.R;
import com.zvestasales.global.BaseActivity;
import com.zvestasales.models.data_model.signupModels.request.ForgotPasswordRequest;
import com.zvestasales.models.data_model.signupModels.request.ResendOtpRequest;
import com.zvestasales.presenters.interfaces.ResetPasswordInterface;
import com.zvestasales.presenters.webservices.ResetPasswordWebservice;
import com.zvestasales.utils.AppUtils;
import com.zvestasales.utils.global_data_utils.IntentKeys;


public class ResetPasswordActivity extends BaseActivity implements ResetPasswordInterface {

    EditText etOtp, etPassword, etConformPassword;
    TextView tvSubmitResetPassword, tvResendOtp;
    Context _context;
    String phoneNumber;
    ResetPasswordWebservice resetPasswordWebservice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        setData();
        checkIntent();
        initializeViews();
        setOnClickViews();
    }

    @Override
    protected void onDestroy() {
        resetPasswordWebservice = null;
        super.onDestroy();
    }

    private void setData(){
        _context = this;
        resetPasswordWebservice = new ResetPasswordWebservice(this, _context);
    }

    private void checkIntent(){
        if(getIntent().getExtras() != null){
            phoneNumber = getIntent().getStringExtra(IntentKeys.INTENT_KEY_PHONE);
        }
    }

    private void initializeViews(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setToolbar(toolbar, "Reset Password", "", 0);
        setToolbar(toolbar, true, -1);
        etOtp = (EditText) findViewById(R.id.etOtp);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etConformPassword = (EditText) findViewById(R.id.etConformPassword);
        tvResendOtp = (TextView) findViewById(R.id.tvResendOtp);
        tvSubmitResetPassword = (TextView) findViewById(R.id.tvSubmitResetPassword);
    }

    private void setOnClickViews(){
        tvSubmitResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValidate())
                    hitServiceForResetPassword();
            }
        });
        tvResendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitServiceForReSendOtp();
            }
        });
    }

    private boolean isValidate(){
        if(AppUtils.isValidText(etOtp.getText().toString().trim(), 5, 6)){
            if (AppUtils.isValidText(etPassword.getText().toString().trim(), 6, 0)) {
                if (etConformPassword.getText().toString().trim().equals(etPassword.getText().toString().trim())) {
                    return true;
                } else
                    showToast(getResources().getString(R.string.signupactivity_cpassword_valid_text));
            } else
                showToast(getResources().getString(R.string.signupactivity_password_valid_text));
        }else
            showToast("Enter valid otp");
        return false;
    }

    private void hitServiceForResetPassword(){
        if (!showProgressDialog(_context)) {
            return;
        }
        ForgotPasswordRequest request = new ForgotPasswordRequest();
        request.mobile = phoneNumber;
        request.otp = etOtp.getText().toString().trim();
        request.password = etPassword.getText().toString().trim();
        resetPasswordWebservice.hitResetPasswordService(request);
    }

    private void hitServiceForReSendOtp(){
        if (!showProgressDialog(_context)) {
            return;
        }
        ResendOtpRequest request = new ResendOtpRequest();
        request.mobile = phoneNumber;
        request.source = "2";
        resetPasswordWebservice.hitResendOtpService(request);
    }

    @Override
    public void submitResetPassword(String successResponseBody) {
        hideProgressDialog();
        showToast("Your password reset successful.");
        setResult(RESULT_OK);
        finish();
    }
}
