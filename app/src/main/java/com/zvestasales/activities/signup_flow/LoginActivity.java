package com.zvestasales.activities.signup_flow;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.zvestasales.R;
import com.zvestasales.activities.koas_flow.KoasHistoryActivity;
import com.zvestasales.global.BaseActivity;
import com.zvestasales.models.data_model.signupModels.request.LoginRequest;
import com.zvestasales.models.data_model.signupModels.response.SignUpResponse;
import com.zvestasales.models.data_model.userDataModels.UserMainData;
import com.zvestasales.presenters.interfaces.LoginInterface;
import com.zvestasales.presenters.webservices.LoginWebservice;
import com.zvestasales.utils.AppUtils;
import com.zvestasales.utils.activity_handler_utils.RegisterSignUpFlowHandler;
import com.zvestasales.utils.global_data_utils.CurrentStateUtil;
import com.zvestasales.utils.global_data_utils.GlobalDataUtils;
import com.zvestasales.utils.global_data_utils.PreferenceService;
import com.zvestasales.utils.validator_utils.EmailValidator;

public class LoginActivity extends BaseActivity implements LoginInterface {

    EditText etUserName, etPassword;
    TextView tvSignInSubmit, tvForgotPass, tvLogin;
    Context _context;
    LoginWebservice loginWebservice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(AppUtils.isNullOrBlankCheck(CurrentStateUtil.getLoginState()) && CurrentStateUtil.getLoginState().equals(GlobalDataUtils.LOGIN_STATE)){
            startActivity(new Intent(LoginActivity.this, KoasHistoryActivity.class));
            finish();
        }
        setContentView(R.layout.activity_login);

        setData();
        initializeViews();
        setOnClickViews();

        hideSoftKeyboard();
    }

    private void setData(){
        RegisterSignUpFlowHandler.register(this);
        _context = this;
        loginWebservice = new LoginWebservice(this, _context);
    }

    private void initializeViews(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setToolbar(toolbar, "Login", "", 0);
//        setToolbar(toolbar, true, -1);
        etUserName = (EditText) findViewById(R.id.etUserName);
        etPassword = (EditText) findViewById(R.id.etPassword);
        tvSignInSubmit = (TextView) findViewById(R.id.tvSignInSubmit);
        tvForgotPass = (TextView) findViewById(R.id.tvForgotPass);
        tvLogin = (TextView) findViewById(R.id.tvLogin);

        SpannableString login = new SpannableString(getResources().getString(R.string.signupactivity_login_text));
        login.setSpan(new ForegroundColorSpan(ContextCompat.getColor(_context, R.color.gradient_center)), 23, login.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvLogin.setText(login);
    }

    private void setOnClickViews(){
        etPassword.setOnEditorActionListener(onDonePressListener);
        tvSignInSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValidateUserData())
                    performSigninOps();
            }
        });
        tvForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performForgotPassword();
            }
        });
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSignUpScreen();
            }
        });
    }

    private void goToSignUpScreen(){
        startActivity(new Intent(_context, SignUpActivity.class));
    }

    private TextView.OnEditorActionListener onDonePressListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

            // when "correct arrow" is pressed then action_done is event occurs
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                    || (actionId == EditorInfo.IME_ACTION_DONE)) {
                if(isValidateUserData())
                    performSigninOps();
            }
            return true;
        }
    };

    private boolean isValidateUserData(){
        if(new EmailValidator().validate(etUserName.getText().toString().trim())){
            if(AppUtils.isValidText(etPassword.getText().toString(), 6, 0)){
                return true;
            }else
                showToast(getResources().getString(R.string.signupactivity_password_valid_text));
        }else
            showToast(getResources().getString(R.string.signupactivity_email_valid_text));
        return false;
    }

    private void performSigninOps(){
        // TODO: hit service for sign in operation.
        if(!showProgressDialog(_context)){
            return;
        }
        LoginRequest request = new LoginRequest();
        request.email = etUserName.getText().toString();
        request.password = etPassword.getText().toString();
        loginWebservice.hitLoginService(request);
    }

    private void performForgotPassword(){
        startActivity(new Intent(_context, ForgotPasswordActivity.class));
    }

    @Override
    public void submitLoginResponse(SignUpResponse responseBody) {
        hideProgressDialog();
        performLogin200Ops(responseBody);
    }

    private void performLogin200Ops(SignUpResponse body) {
        PreferenceService.clear();
        UserMainData userMainData = new UserMainData();
        userMainData.userId = body.SUCCESS.UserData.user_id;
        userMainData.fName = body.SUCCESS.UserData.first_name;
        userMainData.lName = body.SUCCESS.UserData.last_name;
        userMainData.email = body.SUCCESS.UserData.email;
        userMainData.mobile = body.SUCCESS.UserData.mobile;
        userMainData.token = AppUtils.generateBearerAuthorigationToken(body.SUCCESS.token);
        userMainData.userTypeId = body.SUCCESS.UserData.user_type_id;
        userMainData.sourceType = GlobalDataUtils.USER_TYPE_SELLER;
        CurrentStateUtil.putCurrentUserMainData(userMainData);

        if(body.SUCCESS.UserData.is_verified.equals("0"))
            startActivity(new Intent(_context, AccountVerificationActivity.class));
        else{
            showToast("Login successfully completed");
            CurrentStateUtil.putLoginState(GlobalDataUtils.LOGIN_STATE);
            goToKoasHistory();
        }
    }

    private void goToKoasHistory(){
        Intent intent = new Intent(_context, KoasHistoryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        RegisterSignUpFlowHandler.removeAll();
    }

    @Override
    protected void onDestroy() {
        clearMemory();
        super.onDestroy();
    }

    private void clearMemory(){
        loginWebservice = null;
        RegisterSignUpFlowHandler.removeLastActivity();
    }
}
