package com.zvestasales.activities.signup_flow;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.zvestasales.R;
import com.zvestasales.activities.koas_flow.KoasHistoryActivity;
import com.zvestasales.global.BaseActivity;
import com.zvestasales.models.data_model.signupModels.request.ResendOtpRequest;
import com.zvestasales.models.data_model.signupModels.request.UserVerificationRequest;
import com.zvestasales.models.data_model.signupModels.response.UserVerificationResponse;
import com.zvestasales.models.data_model.userDataModels.UserMainData;
import com.zvestasales.presenters.interfaces.AccountVerificationInterface;
import com.zvestasales.presenters.webservices.AccountVerificationWebservice;
import com.zvestasales.utils.AppUtils;
import com.zvestasales.utils.activity_handler_utils.RegisterSignUpFlowHandler;
import com.zvestasales.utils.global_data_utils.CurrentStateUtil;
import com.zvestasales.utils.global_data_utils.GlobalDataUtils;

public class AccountVerificationActivity extends BaseActivity implements AccountVerificationInterface {

    EditText etOtp;
    TextView tvOtpSubmit, tvResendCode;
    Context _context;
    UserMainData userMainData;
    AccountVerificationWebservice accountVerificationWebservice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_verification);

        setData();
        initializeViews();
        setOnClickViews();
    }

    private void setData(){
        RegisterSignUpFlowHandler.register(this);
        _context = this;
        userMainData = CurrentStateUtil.getCurrentUserMainData();
        accountVerificationWebservice = new AccountVerificationWebservice(this, _context);
    }

    private void initializeViews(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setToolbar(toolbar, "Verify Your Account", "", 0);
        setToolbar(toolbar, true, -1);
        etOtp = (EditText) findViewById(R.id.etOtp);
        tvOtpSubmit = (TextView) findViewById(R.id.tvOtpSubmit);
        tvResendCode = (TextView) findViewById(R.id.tvResendCode);
    }

    private void setOnClickViews(){
        tvOtpSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpOperation();
            }
        });
        tvResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitServiceForResendCode();
            }
        });
    }

    private void hitServiceForResendCode(){
        if (!showProgressDialog(_context)) {
            return;
        }
        ResendOtpRequest request = new ResendOtpRequest();
        request.mobile = userMainData.mobile;
        request.source = "1";
        accountVerificationWebservice.hitResendOtpService(request);
    }

    private void otpOperation(){
        if (!showProgressDialog(_context)) {
            return;
        }
        UserVerificationRequest request = new UserVerificationRequest();
        request.mobile = userMainData.mobile;
        request.otp = etOtp.getText().toString().trim();
        accountVerificationWebservice.hitUserVerificationService(request);
    }

    @Override
    public void submitUserVerification(UserVerificationResponse responseBody) {
        hideProgressDialog();
        showToast("Verification successfully completed");
        userMainData.token = AppUtils.generateBearerAuthorigationToken(responseBody.SUCCESS.UserData.token);
        CurrentStateUtil.putCurrentUserMainData(userMainData);
        CurrentStateUtil.putLoginState(GlobalDataUtils.LOGIN_STATE);
        Intent intent = new Intent(_context, KoasHistoryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        RegisterSignUpFlowHandler.removeAll();
    }

    @Override
    protected void onDestroy() {
        clearMemory();
        super.onDestroy();
    }

    private void clearMemory(){
        accountVerificationWebservice = null;
        RegisterSignUpFlowHandler.removeLastActivity();
    }
}
