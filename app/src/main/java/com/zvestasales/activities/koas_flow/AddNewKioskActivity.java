package com.zvestasales.activities.koas_flow;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.zvestasales.R;
import com.zvestasales.global.BaseActivity;
import com.zvestasales.models.data_model.koasModels.request.EnqueryRequest;
import com.zvestasales.models.data_model.koasModels.response.EnqueryDetailsModel;
import com.zvestasales.models.data_model.koasModels.response.EnqueryResponse;
import com.zvestasales.presenters.interfaces.AddLeadsInterface;
import com.zvestasales.presenters.webservices.AddLeadsWebservice;
import com.zvestasales.utils.AppUtils;
import com.zvestasales.utils.global_data_utils.CurrentStateUtil;
import com.zvestasales.utils.global_data_utils.IntentKeys;
import com.zvestasales.utils.global_data_utils.RequestCodes;
import com.zvestasales.utils.validator_utils.EmailValidator;
import com.zvestasales.utils.validator_utils.PhoneValidator;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class AddNewKioskActivity extends BaseActivity implements AddLeadsInterface {

    EditText etFName, etLName, etPhone, etEmail, etComments, etBudget, etOccupation, etCity;
    TextView tvLeadSubmit, etBestTime;
    RadioGroup rgGender;
    RadioButton rbMale, rbFemale;
    Spinner spMarriedStatus;

    Context _context;
    AddLeadsWebservice addLeadsWebservice;
    String projectId, gender = "", maritalStatus = "";
    EnqueryDetailsModel kioskDetails;
    int mHour, mMinute;
    GregorianCalendar mCalendar;
    String[] MARRIEDLIST = {"Select marital status", "Married", "Unmarried", "Others"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lead);

        setData();
        initializeViews();
        setOnClickViews();

        hideSoftKeyboard();
    }

    private void setData() {
        _context = this;
        addLeadsWebservice = new AddLeadsWebservice(this, _context);
        mCalendar = new GregorianCalendar();
    }

    private void initializeViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setToolbar(toolbar, "Add Kiosk", "", 0);
        setToolbar(toolbar, true, -1);
        etFName = (EditText) findViewById(R.id.etFName);
        etLName = (EditText) findViewById(R.id.etLName);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etComments = (EditText) findViewById(R.id.etComments);
        etBudget = (EditText) findViewById(R.id.etBudget);
        etBestTime = (TextView) findViewById(R.id.etBestTime);
        etOccupation = (EditText) findViewById(R.id.etOccupation);
        etCity = (EditText) findViewById(R.id.etCity);
        tvLeadSubmit = (TextView) findViewById(R.id.tvLeadSubmit);
        rgGender = (RadioGroup) findViewById(R.id.rgGender);
        rbMale = (RadioButton) findViewById(R.id.rbMale);
        rbFemale = (RadioButton) findViewById(R.id.rbFemale);

        mCalendar.setTimeInMillis(System.currentTimeMillis());
        mHour = mCalendar.get(Calendar.HOUR_OF_DAY);
        mMinute = mCalendar.get(Calendar.MINUTE);
        intializeSpinner();
    }

    private void intializeSpinner(){
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, MARRIEDLIST);
        spMarriedStatus = (Spinner)
                findViewById(R.id.spMarriedStatus);
        spMarriedStatus.setAdapter(arrayAdapter);
    }

    private void setOnClickViews() {
        tvLeadSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidate())
                    hitAddLeadsService();
            }
        });
        etBestTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(_context, mTimeSetListener, mHour, mMinute, false).show();
            }
        });
        spMarriedStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position >= 0){
                    if(MARRIEDLIST[position].equals("Married"))
                        maritalStatus = "M";
                    else if(MARRIEDLIST[position].equals("Unmarried"))
                        maritalStatus = "U";
                    else if(MARRIEDLIST[position].equals("Others"))
                        maritalStatus = "O";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /*Spinner start time select listener*/
    private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            etBestTime.setText(new DecimalFormat("00").format(hourOfDay) + ":" + new DecimalFormat("00").format(minute) + ":00");
        }
    };

    private boolean isValidate() {
        int selectedId = rgGender.getCheckedRadioButtonId();
        // find which radioButton is checked by id
        if (selectedId == rbMale.getId())
            gender = "M";
        else if (selectedId == rbFemale.getId())
            gender = "F";
        if (AppUtils.isValidText(etFName.getText().toString().trim(), 3, 25) && AppUtils.isValidText(etLName.getText().toString().trim(), 3, 25)) {
            if (AppUtils.isValidText(etPhone.getText().toString().trim(), 10, 10) && new PhoneValidator().validate(etPhone.getText().toString().trim())) {
                if (!etEmail.getText().toString().trim().equals("") && new EmailValidator().validate(etEmail.getText().toString().trim())) {
                    if (AppUtils.isNullOrBlankCheck(gender)) {
                        if (!AppUtils.isNullOrBlankCheck(etBudget.getText().toString()) || AppUtils.isValidText(etBudget.getText().toString(), 0, 10)) {
                            return true;
                        } else
                            showToast("Budget limit should be 10 digit");
                    } else
                        showToast("Select your gender.");
                } else
                    showToast(getResources().getString(R.string.signupactivity_email_valid_text));
            } else
                showToast(getResources().getString(R.string.signupactivity_phone_valid_text));
        } else
            showToast("Please enter valid name");
        return false;
    }

    /*private boolean isCheckBestTime() {
        if (AppUtils.isNullOrBlankCheck(etBestTime.getText().toString())) {
            String bestTime = etBestTime.getText().toString().replace(":", "");
            if (bestTime.length() > 6)
                return false;
            else
                return true;
        } else {
            return true;
        }
    }*/

    private void hitAddLeadsService() {
        if (!showProgressDialog(_context)) {
            return;
        }

        EnqueryRequest request = new EnqueryRequest();
        request.first_name = etFName.getText().toString().trim();
        request.last_name = etLName.getText().toString().trim();
        request.mobile = etPhone.getText().toString().trim();
        request.email = etEmail.getText().toString().trim();
        request.comment = etComments.getText().toString().trim();
        request.budget = etBudget.getText().toString().trim();
        request.best_time = etBestTime.getText().toString().trim();
        request.occupation = etOccupation.getText().toString().trim();
        request.city = etCity.getText().toString().trim();
        request.gender = gender;
        request.marital_status = maritalStatus;
        addLeadsWebservice.hitAddLeadsService(CurrentStateUtil.getCurrentUserMainData().token, request);
    }

    @Override
    public void submitAddLeadResponse(EnqueryResponse responseBody) {
        hideProgressDialog();
        kioskDetails = responseBody.SUCCESS;
        Intent intent = new Intent(_context, KioskVerificationActivity.class);
        intent.putExtra(IntentKeys.INTENT_KEY_PHONE, responseBody.SUCCESS.mobile);
        startActivityForResult(intent, RequestCodes.KIOSK_VERIFICATION_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RequestCodes.KIOSK_VERIFICATION_REQUEST_CODE && resultCode == RESULT_OK) {
            String leadDetails = gson.toJson(kioskDetails);
            Intent intent = new Intent();
            intent.putExtra(IntentKeys.INTENT_KEY_ADD_KIOSK_DATA, leadDetails);
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
