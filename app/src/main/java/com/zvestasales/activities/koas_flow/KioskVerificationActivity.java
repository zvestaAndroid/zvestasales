package com.zvestasales.activities.koas_flow;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.zvestasales.R;
import com.zvestasales.global.BaseActivity;
import com.zvestasales.models.data_model.signupModels.request.ResendOtpRequest;
import com.zvestasales.models.data_model.signupModels.request.UserVerificationRequest;
import com.zvestasales.models.data_model.signupModels.response.UserVerificationResponse;
import com.zvestasales.presenters.interfaces.AccountVerificationInterface;
import com.zvestasales.presenters.webservices.AccountVerificationWebservice;
import com.zvestasales.utils.AppUtils;
import com.zvestasales.utils.activity_handler_utils.RegisterSignUpFlowHandler;
import com.zvestasales.utils.global_data_utils.IntentKeys;

public class KioskVerificationActivity extends BaseActivity implements AccountVerificationInterface {

    EditText etOtp;
    TextView tvOtpSubmit, tvResendCode;
    Context _context;
    AccountVerificationWebservice accountVerificationWebservice;
    String mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_verification);

        checkIntent();
        setData();
        initializeViews();
        setOnClickViews();
    }

    private void checkIntent(){
        if(getIntent().getExtras() != null){
            mobile = getIntent().getStringExtra(IntentKeys.INTENT_KEY_PHONE);
        }
    }

    private void setData(){
        RegisterSignUpFlowHandler.register(this);
        _context = this;
        accountVerificationWebservice = new AccountVerificationWebservice(this, _context);
    }

    private void initializeViews(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setToolbar(toolbar, "Verify Kiosk Account", "", 0);
        setToolbar(toolbar, true, -1);
        etOtp = (EditText) findViewById(R.id.etOtp);
        tvOtpSubmit = (TextView) findViewById(R.id.tvOtpSubmit);
        tvResendCode = (TextView) findViewById(R.id.tvResendCode);
    }

    private void setOnClickViews(){
        tvOtpSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpOperation();
            }
        });
        tvResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitServiceForResendCode();
            }
        });
    }

    private void hitServiceForResendCode(){
        if(!AppUtils.isNullOrBlankCheck(mobile)) {
            showToast("Mobile number return null");
            return;
        }
        if (!showProgressDialog(_context)) {
            return;
        }
        ResendOtpRequest request = new ResendOtpRequest();
        request.mobile = mobile;
        request.source = "1";
        accountVerificationWebservice.hitResendOtpService(request);
    }

    private void otpOperation(){
        if(!AppUtils.isNullOrBlankCheck(mobile)) {
            showToast("Mobile number return null");
            return;
        }
        if (!showProgressDialog(_context)) {
            return;
        }
        UserVerificationRequest request = new UserVerificationRequest();
        request.mobile = mobile;
        request.otp = etOtp.getText().toString().trim();
        accountVerificationWebservice.hitUserVerificationService(request);
    }

    @Override
    public void submitUserVerification(UserVerificationResponse responseBody) {
        hideProgressDialog();
        showToast("Kiosk verification successfully completed");
        setResult(RESULT_OK);
        finish();
    }

    @Override
    protected void onDestroy() {
        accountVerificationWebservice = null;
        super.onDestroy();
    }
}
