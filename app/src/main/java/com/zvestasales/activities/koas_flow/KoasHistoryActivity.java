package com.zvestasales.activities.koas_flow;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.getbase.floatingactionbutton.AddFloatingActionButton;
import com.zvestasales.R;
import com.zvestasales.adapters.KoasHistoryAdapter;
import com.zvestasales.customviews.NotificationDialogs;
import com.zvestasales.global.BaseActivity;
import com.zvestasales.models.data_model.koasModels.response.EnqueryDetailsModel;
import com.zvestasales.models.data_model.koasModels.response.EnqueryHistoryResponse;
import com.zvestasales.models.data_model.userDataModels.UserMainData;
import com.zvestasales.presenters.interfaces.EnqueryHistoryInterface;
import com.zvestasales.presenters.webservices.EnqueryHistoryWebservice;
import com.zvestasales.utils.AppUtils;
import com.zvestasales.utils.global_data_utils.CurrentStateUtil;
import com.zvestasales.utils.global_data_utils.IntentKeys;
import com.zvestasales.utils.global_data_utils.RequestCodes;
import com.zvestasales.utils.swipy_refresh_lib.SwipyRefreshLayout;
import com.zvestasales.utils.swipy_refresh_lib.SwipyRefreshLayoutDirection;

import java.util.ArrayList;
import java.util.List;

public class KoasHistoryActivity extends BaseActivity implements EnqueryHistoryInterface, NotificationDialogs.NotificationCallbacks {

    Toolbar toolbar;
    TextView tvEmptyMsg, tvLogOut;
    LinearLayout llList;
    KoasHistoryAdapter historyAdapter;

    Context _context;
    List<EnqueryDetailsModel> historyList = new ArrayList<>();
    EnqueryHistoryWebservice enqueryHistoryWebservice;
    UserMainData userMainData;
    AddFloatingActionButton floatingButton;
    SwipyRefreshLayout swipeView;
    int currentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        setData();
        initializeViews();
        setOnClickViews();

        hitServiceEnqueryHistory();
    }

    private void setData(){
        _context = this;
        userMainData = CurrentStateUtil.getCurrentUserMainData();
        enqueryHistoryWebservice = new EnqueryHistoryWebservice(_context);
    }

    private void initializeViews(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setToolbar(toolbar, "Kiosk History", "Logout", 0);
//        setToolbar(toolbar, true, "");
        tvLogOut = (TextView) findViewById(R.id.tvToolbarSave);
        tvEmptyMsg = (TextView) findViewById(R.id.tvEmptyMsg);
        llList = (LinearLayout) findViewById(R.id.llList);
        floatingButton = (AddFloatingActionButton) findViewById(R.id.floatingButton);
        swipeView = (SwipyRefreshLayout) findViewById(R.id.swipeView);
        swipeView.setDirection(SwipyRefreshLayoutDirection.BOTTOM);
        initializeRecylerView();
    }

    private void initializeRecylerView(){
        RecyclerView rvHistory = (RecyclerView) findViewById(R.id.rvHistory);
        historyAdapter = new KoasHistoryAdapter(_context, historyList);
        LinearLayoutManager ooLayoutManagaer = new LinearLayoutManager(_context);
        rvHistory.setLayoutManager(ooLayoutManagaer);
        rvHistory.setHasFixedSize(true);
        rvHistory.setAdapter(historyAdapter);
    }

    private void showExitAlertDialog(){
        new NotificationDialogs(this).showDefaultAlertDialog(_context, "zVesta sales exit", "Are you sure you want to close zVesta sales?", "", "", "Exit");
    }

    private void setOnClickViews(){
        tvLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutAlert();
            }
        });
        floatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(_context, AddNewKioskActivity.class), RequestCodes.ADD_KIOSK_REQUEST_CODE);
            }
        });
        swipeView.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                checkForPagination();
            }
        });
        /*toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showExitAlertDialog();
            }
        });*/

    }

    @Override
    public void onBackPressed() {
        showExitAlertDialog();
//        super.onBackPressed();
    }

    private void logoutAlert(){
        new NotificationDialogs(this).showDefaultAlertDialog(_context, getResources().getString(R.string.alert_title_logout), getResources().getString(R.string.alert_message_logout), "", "", "Logout");
    }

    private void hitServiceEnqueryHistory(){
        if(!showProgressDialog(_context))
            return;
        enqueryHistoryWebservice.hitEnqueryHistoryService(userMainData.token, currentPage);
    }

    private void checkForPagination(){
        hitServiceEnqueryHistory();
    }

    private void addNewLeads(EnqueryDetailsModel addNewItem){
        historyList.add(addNewItem);
        if(tvEmptyMsg.getVisibility() == View.VISIBLE){
            tvEmptyMsg.setVisibility(View.GONE);
            llList.setVisibility(View.VISIBLE);
        }
        historyAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            switch (requestCode){
                case RequestCodes.ADD_KIOSK_REQUEST_CODE:
                    String addLeads = data.getStringExtra(IntentKeys.INTENT_KEY_ADD_KIOSK_DATA);
                    EnqueryDetailsModel enqueryDetailsModel = gson.fromJson(addLeads, EnqueryDetailsModel.class);
                    addNewLeads(enqueryDetailsModel);
                    break;
            }
        }
    }

    @Override
    public void enqueryHistoryResponse(EnqueryHistoryResponse responseBody) {
        hideProgressDialog();
        if(responseBody.SUCCESS.enquiries != null && responseBody.SUCCESS.enquiries.size() > 0){
            tvEmptyMsg.setVisibility(View.GONE);
            llList.setVisibility(View.VISIBLE);
            historyList.addAll(responseBody.SUCCESS.enquiries);
            historyAdapter.notifyDataSetChanged();

            swipeView.setRefreshing(false);
            if(responseBody.SUCCESS.enquiries.size() < 30){
                swipeView.setEnabled(false);
            }else{
                currentPage++;
            }
        }else{
            tvEmptyMsg.setVisibility(View.VISIBLE);
            llList.setVisibility(View.GONE);
            swipeView.setRefreshing(false);
            swipeView.setEnabled(false);
        }
    }

    @Override
    public void logoutSuccessOps() {
        hideProgressDialog();
        AppUtils.logoutLocally(_context);
        finish();
    }

    @Override
    public void onYesPressed(String type) {

        if(type.equals("Exit")){
            finish();
        }else{
            if(!showProgressDialog(_context)){
                return;
            }
            enqueryHistoryWebservice.hitLogoutService(userMainData.token);
        }
    }

    @Override
    public void onNoPressed(String type) {
    }

    public void showDialogForKioskDetails(int position){
        EnqueryDetailsModel item = historyList.get(position);
        final Dialog dialog = new Dialog(_context);
        dialog.setContentView(R.layout.kiosk_details_layout);

        TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        TextView tvSmallDesc = (TextView) dialog.findViewById(R.id.tvSmallDesc);
        TextView tvProjectName = (TextView) dialog.findViewById(R.id.tvSellerName);
        TextView tvCity = (TextView) dialog.findViewById(R.id.tvCity);
        TextView tvMaxBudget = (TextView) dialog.findViewById(R.id.tvMaxBudget);
        TextView tvComments = (TextView) dialog.findViewById(R.id.tvComments);
        TextView tvBestTime = (TextView) dialog.findViewById(R.id.tvBestTime);
        Button btDiamiss = (Button) dialog.findViewById(R.id.btDiamiss);

        tvTitle.setText("Kiosk request for " + item.name);
        tvSmallDesc.setText(item.name + " is submitted a kiosk request for the following period " + item.created_at);
        tvProjectName.setText(item.name);
        tvCity.setText(AppUtils.isNullOrBlankCheck(item.city)? item.city : "");
        tvMaxBudget.setText(AppUtils.isNullOrBlankCheck(item.budget)? item.budget : "00000000");
        tvComments.setText(AppUtils.isNullOrBlankCheck(item.comment)? item.comment : "");
        tvBestTime.setText(AppUtils.isNullOrBlankCheck(item.best_time)? item.best_time : "00:00:00");

        btDiamiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
