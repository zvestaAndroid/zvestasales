package com.zvestasales.utils.global_data_utils;

public class RequestCodes {

    // Sign up flow
    public static final int FORGOT_PASSWORD_REQUEST_CODE = 15;
    public static final int ADD_KIOSK_REQUEST_CODE = 16;
    public static final int KIOSK_VERIFICATION_REQUEST_CODE = 17;
}
