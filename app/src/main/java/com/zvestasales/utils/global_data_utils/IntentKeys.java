package com.zvestasales.utils.global_data_utils;

public class IntentKeys {
    public static final String INTENT_KEY_EMAIL = "email";
    public static final String INTENT_KEY_NAME = "name";
    public static final String INTENT_KEY_PHONE = "phone";
    public static final String INTENT_KEY_ID = "id";
    public static final String INTENT_KEY_ADD_KIOSK_DATA = "addKioskData";
}
