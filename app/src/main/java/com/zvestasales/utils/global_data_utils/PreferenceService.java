package com.zvestasales.utils.global_data_utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.zvestasales.global.BaseApplication;


public class PreferenceService {

    public static final String GLOBAL_DEFAULT_VALUE = "defaultValue";

    public static final String GLOBAL_CURRENT_LOGIN_STATE = "currentLoginState";
    public static final String GLOBAL_USER_MAIN_DATA_STRING = "userMainData";

    public final static String appPreference = "appPreference";

    private static SharedPreferences getPrefs() {
        return BaseApplication.getAppContext().getSharedPreferences(appPreference, Context.MODE_PRIVATE);
    }

    public static boolean putString(String keyValue, String valueText) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString(keyValue, valueText);
        return editor.commit();
    }

    public static String getString(String keyValue) {
        return getPrefs().getString(keyValue, GLOBAL_DEFAULT_VALUE);
    }

    public static boolean clear() {
        SharedPreferences.Editor editor = getPrefs().edit();
        return editor.clear().commit();
    }
}
