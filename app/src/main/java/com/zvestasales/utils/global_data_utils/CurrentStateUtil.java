package com.zvestasales.utils.global_data_utils;

import com.google.gson.Gson;
import com.zvestasales.models.data_model.userDataModels.UserMainData;

public class CurrentStateUtil {

    private static Gson gson = new Gson();

    public static String getLoginState() {
        return PreferenceService.getString(PreferenceService.GLOBAL_CURRENT_LOGIN_STATE);
    }

    public static void putLoginState(String loginState) {
        PreferenceService.putString(PreferenceService.GLOBAL_CURRENT_LOGIN_STATE, loginState);
    }

    public static UserMainData getCurrentUserMainData() {
        String userDataString = PreferenceService.getString(PreferenceService.GLOBAL_USER_MAIN_DATA_STRING);
        if (userDataString != null && !userDataString.equals("defaultValue") && !userDataString.equals("")){
            return gson.fromJson(userDataString, UserMainData.class);
        }else{
            return null;
        }
    }

    public static void putCurrentUserMainData(UserMainData userMainData) {
        String dataString = gson.toJson(userMainData);
        PreferenceService.putString(PreferenceService.GLOBAL_USER_MAIN_DATA_STRING, dataString);
    }
}
