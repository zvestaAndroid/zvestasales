package com.zvestasales.utils.global_data_utils;

public class GlobalDataUtils {

    /*Base url for network call*/
    public static final String BASE_API = "http://dev.zvesta.com/api/";
//    public static final String BASE_API = "http://test.zvesta.com/api/";

    /*Different type of user type with id*/
    public static final String USER_TYPE_SELLER = "4";

    /*********User source type********/
    public static final String SOURCE_TYPE_DEFAULT = "1";

    /*********User login status********/
    public static final String LOGIN_STATE = "login";

    // Response validation
    public static final String API_STATUS = "SUCCESS";
}
