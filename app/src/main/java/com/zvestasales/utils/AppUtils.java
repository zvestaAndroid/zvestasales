package com.zvestasales.utils;

import android.content.Context;
import android.content.Intent;

import com.zvestasales.activities.signup_flow.LoginActivity;
import com.zvestasales.global.BaseActivity;
import com.zvestasales.models.error.ApiError;
import com.zvestasales.utils.error_utils.ErrorUtil;
import com.zvestasales.utils.global_data_utils.GlobalDataUtils;
import com.zvestasales.utils.global_data_utils.PreferenceService;

import retrofit2.Response;

/**
 * Created by test on 11/14/2017.
 */

public class AppUtils {

    public static boolean isValidText(String inputStr, int minLength, int maxLength){
        if(inputStr != null && !inputStr.equals("") && inputStr.length() >= minLength && (maxLength == 0 || inputStr.length() <= maxLength)){
            return true;
        }
        return false;
    }

    // Check api status for response is valid or not
    public static boolean isValidResponse(String apiStatus){
        if(apiStatus != null && apiStatus.equals(GlobalDataUtils.API_STATUS))
            return true;
        return false;
    }

    public static void showApiError(Context _context, String errorMessage){
        if(errorMessage != null)
            ((BaseActivity) _context).showToast("" + errorMessage);
        else
            ((BaseActivity) _context).showToast("Some thing wrong please try again later.");
    }

    // when response is not 200 then show the message
    public static void showError(Context _context, Response<?> response) {
        ApiError error = ErrorUtil.parseError(response);
        if (error != null && error.message != null) {
            AppUtils.performBadRequestOps(_context, error.statusCode, error.message);
        } else {
            AppUtils.performBadRequestOps(_context, response.code());
        }
    }

    //  Show error code for network calling
    public static void performBadRequestOps(Context context, int statusCode) {
        switch (statusCode) {
            case 400:
                AppUtils.logoutLocally(context);
                break;
            case 401:
                AppUtils.logoutLocally(context);
                break;
            default:
                ((BaseActivity) context).showToast(Integer.toString(statusCode));
        }
    }

    // Show error message for network calling
    public static void performBadRequestOps(Context context, int statusCode, String message) {
        switch (statusCode) {
            case 400:
                AppUtils.logoutLocally(context);
                break;
            case 401:
                AppUtils.logoutLocally(context);
            default:
                ((BaseActivity) context).showToast("" + message);
        }
    }

    public static void logoutLocally(Context _context) {
        PreferenceService.clear();
        Intent intent = new Intent(_context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        _context.startActivity(intent);
    }

    public static boolean isNullOrBlankCheck(String value){
        if(value != null && !value.equals("") && !value.equals("null") && !value.equals("NULL") && !value.equals("defaultValue"))
            return true;
        else
            return false;
    }

    public static String generateBearerAuthorigationToken(String token) {
        return "Bearer " + token;
    }
}
