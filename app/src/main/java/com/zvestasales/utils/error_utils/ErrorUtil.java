package com.zvestasales.utils.error_utils;


import com.zvestasales.models.error.ApiError;
import com.zvestasales.network.JSONService;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtil {

    public static ApiError parseError(Response<?> response) {

        Converter<ResponseBody, ApiError> converter =
                JSONService.restAdapter
                        .responseBodyConverter(ApiError.class, new Annotation[0]);
        ApiError error;
        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ApiError();
        }
        return error;
    }

}
