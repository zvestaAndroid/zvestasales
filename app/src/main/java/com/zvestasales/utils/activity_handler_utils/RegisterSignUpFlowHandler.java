package com.zvestasales.utils.activity_handler_utils;

import android.app.Activity;

import java.util.ArrayList;

/**
 * Created by test on 10/9/2017.
 */

public class RegisterSignUpFlowHandler {
    // This array store all activity
    public static ArrayList<Activity> registerArayList = new ArrayList<Activity>();

    /**
     * Store activity
     * @param activity
     */
    public static void register(Activity activity) {
        registerArayList.add(activity);
    }

    /**
     * Remove all activity.
     */
    public static void removeAll() {

        for (int i = 0; i < registerArayList.size(); i++) {
            try {
                registerArayList.get(i).finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        registerArayList.clear();
    }

    public static void removeLastActivity(){
        if(registerArayList.size() > 0){
            registerArayList.remove(registerArayList.size() - 1);
        }
    }
    /**
     * Return top activity.
     * @return top activity.
     */
    public static Activity getLastActivity() {
        return registerArayList.get(registerArayList.size() - 1);
    }
}
