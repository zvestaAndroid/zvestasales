package com.zvestasales.utils.validator_utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by test on 7/27/2017.
 */

public class PhoneValidator {

    private Pattern pattern;
    private Matcher matcher;

    private static final String PHONE_REGEX = "\\d{10}";

    public PhoneValidator() {
        pattern = Pattern.compile(PHONE_REGEX);
    }

    /**
     * Validate hex with regular expression
     *
     * @param hex hex for validation
     * @return true valid hex, false invalid hex
     */
    public boolean validate(final String hex) {

        matcher = pattern.matcher(hex);
        return matcher.matches();

    }
}
