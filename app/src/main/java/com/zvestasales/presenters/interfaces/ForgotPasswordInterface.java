package com.zvestasales.presenters.interfaces;

/**
 * Created by test on 10/24/2017.
 */

public interface ForgotPasswordInterface {

    void receiveOtp(String successResponseBody);
}
