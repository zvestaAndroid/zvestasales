package com.zvestasales.presenters.interfaces;


import com.zvestasales.models.data_model.koasModels.response.EnqueryResponse;

/**
 * Created by test on 10/26/2017.
 */

public interface AddLeadsInterface {
    void submitAddLeadResponse(EnqueryResponse responseBody);
}
