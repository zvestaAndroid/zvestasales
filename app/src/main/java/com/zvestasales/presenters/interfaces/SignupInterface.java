package com.zvestasales.presenters.interfaces;


import com.zvestasales.models.data_model.signupModels.response.SignUpResponse;

/**
 * Created by test on 10/9/2017.
 */

public interface SignupInterface {

    void submitSignUpResponse(SignUpResponse responseBody);
}
