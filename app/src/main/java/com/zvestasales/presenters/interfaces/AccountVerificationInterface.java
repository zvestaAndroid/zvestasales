package com.zvestasales.presenters.interfaces;


import com.zvestasales.models.data_model.signupModels.response.UserVerificationResponse;

/**
 * Created by test on 10/13/2017.
 */

public interface AccountVerificationInterface {
    void submitUserVerification(UserVerificationResponse responseBody);
}
