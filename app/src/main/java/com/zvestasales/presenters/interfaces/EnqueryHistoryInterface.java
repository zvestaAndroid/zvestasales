package com.zvestasales.presenters.interfaces;


import com.zvestasales.models.data_model.koasModels.response.EnqueryHistoryResponse;

/**
 * Created by test on 10/30/2017.
 */

public interface EnqueryHistoryInterface {
    void enqueryHistoryResponse(EnqueryHistoryResponse responseBody);
    void logoutSuccessOps();
}
