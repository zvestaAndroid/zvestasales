package com.zvestasales.presenters.interfaces;

/**
 * Created by test on 10/24/2017.
 */

public interface ResetPasswordInterface {
    void submitResetPassword(String successResponseBody);
}
