package com.zvestasales.presenters.webservices;

import android.content.Context;

import com.zvestasales.global.BaseActivity;
import com.zvestasales.models.data_model.SuccessResponse;
import com.zvestasales.models.data_model.signupModels.request.ResendOtpRequest;
import com.zvestasales.models.data_model.signupModels.request.UserVerificationRequest;
import com.zvestasales.models.data_model.signupModels.response.UserVerificationResponse;
import com.zvestasales.presenters.interfaces.AccountVerificationInterface;
import com.zvestasales.utils.AppUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by test on 10/13/2017.
 */

public class AccountVerificationWebservice {

    AccountVerificationInterface accountVerificationInterface;
    Context _context;

    public AccountVerificationWebservice(AccountVerificationInterface accountVerificationInterface, Context context){
        this.accountVerificationInterface = accountVerificationInterface;
        this._context = context;
    }

    public void hitUserVerificationService(UserVerificationRequest request){
        Call<UserVerificationResponse> call = ((BaseActivity)_context).jsonService.userVerificationRequest(request);
        call.enqueue(new Callback<UserVerificationResponse>() {
            @Override
            public void onResponse(Call<UserVerificationResponse> call, Response<UserVerificationResponse> response) {
                if (response.code() == 200) {
                    if (AppUtils.isValidResponse(response.body().API_STATUS)) {
                        accountVerificationInterface.submitUserVerification(response.body());
                    } else {
                        ((BaseActivity)_context).hideProgressDialog();
                        AppUtils.showApiError(_context, response.body().ERROR.MESSAGES);
                    }
                } else {
                    ((BaseActivity)_context).hideProgressDialog();
                    AppUtils.showError(_context, response);
                }
            }

            @Override
            public void onFailure(Call<UserVerificationResponse> call, Throwable t) {
                ((BaseActivity)_context).hideProgressDialog();
                ((BaseActivity)_context).showErrorToast(t.getMessage());
            }
        });
    }

    public void hitResendOtpService(ResendOtpRequest request){
        Call<SuccessResponse> call = ((BaseActivity)_context).jsonService.resendOtpRequest(request);
        call.enqueue(new Callback<SuccessResponse>() {
            @Override
            public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
                ((BaseActivity)_context).hideProgressDialog();
                if (response.code() == 200) {
                    if (AppUtils.isValidResponse(response.body().API_STATUS)) {
                        ((BaseActivity)_context).showToast("Please check your mobile number for verification.");
                    } else {
                        AppUtils.showApiError(_context, response.body().ERROR.MESSAGES);
                    }
                } else {
                    AppUtils.showError(_context, response);
                }
            }

            @Override
            public void onFailure(Call<SuccessResponse> call, Throwable t) {
                ((BaseActivity)_context).hideProgressDialog();
                ((BaseActivity)_context).showErrorToast(t.getMessage());
            }
        });
    }
}
