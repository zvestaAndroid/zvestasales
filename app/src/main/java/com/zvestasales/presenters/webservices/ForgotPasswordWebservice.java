package com.zvestasales.presenters.webservices;

import android.content.Context;

import com.zvestasales.global.BaseActivity;
import com.zvestasales.models.data_model.SuccessResponse;
import com.zvestasales.models.data_model.signupModels.request.ForgotPassOtpRequest;
import com.zvestasales.presenters.interfaces.ForgotPasswordInterface;
import com.zvestasales.utils.AppUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by test on 10/24/2017.
 */

public class ForgotPasswordWebservice {

    Context _context;
    ForgotPasswordInterface forgotPasswordInterface;

    public ForgotPasswordWebservice(ForgotPasswordInterface forgotPasswordInterface, Context context){
        this.forgotPasswordInterface = forgotPasswordInterface;
        this._context = context;
    }

    public void hitResendOtpService(ForgotPassOtpRequest request){
        Call<SuccessResponse> call = ((BaseActivity)_context).jsonService.forgotPassOtpRequest(request);
        call.enqueue(new Callback<SuccessResponse>() {
            @Override
            public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
                if (response.code() == 200) {
                    if (AppUtils.isValidResponse(response.body().API_STATUS)) {
                        forgotPasswordInterface.receiveOtp(response.body().API_STATUS);
                    } else {
                        ((BaseActivity)_context).hideProgressDialog();
                        AppUtils.showApiError(_context, response.body().ERROR.MESSAGES);
                    }
                } else {
                    ((BaseActivity)_context).hideProgressDialog();
                    AppUtils.showError(_context, response);
                }
            }

            @Override
            public void onFailure(Call<SuccessResponse> call, Throwable t) {
                ((BaseActivity)_context).hideProgressDialog();
                ((BaseActivity)_context).showErrorToast(t.getMessage());
            }
        });
    }
}

