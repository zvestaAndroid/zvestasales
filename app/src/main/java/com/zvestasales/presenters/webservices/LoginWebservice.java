package com.zvestasales.presenters.webservices;

import android.content.Context;

import com.zvestasales.global.BaseActivity;
import com.zvestasales.models.data_model.signupModels.request.LoginRequest;
import com.zvestasales.models.data_model.signupModels.response.SignUpResponse;
import com.zvestasales.presenters.interfaces.LoginInterface;
import com.zvestasales.utils.AppUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by test on 10/9/2017.
 */

public class LoginWebservice {

    LoginInterface loginInterface;
    Context _context;

    public LoginWebservice(LoginInterface loginInterface, Context context){
        this.loginInterface = loginInterface;
        this._context = context;
    }

    public void hitLoginService(LoginRequest request){
        Call<SignUpResponse> call = ((BaseActivity)_context).jsonService.loginRequest(request);
        call.enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                ((BaseActivity)_context).hideProgressDialog();
                if (response.code() == 200) {
                    if (AppUtils.isValidResponse(response.body().API_STATUS)) {
                        loginInterface.submitLoginResponse(response.body());
                    } else {
                        AppUtils.showApiError(_context, response.body().ERROR.MESSAGES);
                    }
                } else {
                    ((BaseActivity)_context).hideProgressDialog();
                    AppUtils.showError(_context, response);
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                ((BaseActivity)_context).hideProgressDialog();
                ((BaseActivity)_context).showErrorToast(t.getMessage());
            }
        });
    }
}
