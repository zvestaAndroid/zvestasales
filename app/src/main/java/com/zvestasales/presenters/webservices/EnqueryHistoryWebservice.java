package com.zvestasales.presenters.webservices;

import android.content.Context;

import com.zvestasales.global.BaseActivity;
import com.zvestasales.models.data_model.SuccessResponse;
import com.zvestasales.models.data_model.koasModels.response.EnqueryHistoryResponse;
import com.zvestasales.presenters.interfaces.EnqueryHistoryInterface;
import com.zvestasales.utils.AppUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by test on 10/30/2017.
 */

public class EnqueryHistoryWebservice {

    EnqueryHistoryInterface enqueryHistoryInterface;
    Context _context;

    public EnqueryHistoryWebservice(Context context){
        this._context = context;
        this.enqueryHistoryInterface = (EnqueryHistoryInterface)context;
    }

    public void hitEnqueryHistoryService(String token, int pageNo){
        Call<EnqueryHistoryResponse> call = ((BaseActivity)_context).jsonService.koasHistoryRequest(token, pageNo);
        call.enqueue(new Callback<EnqueryHistoryResponse>() {
            @Override
            public void onResponse(Call<EnqueryHistoryResponse> call, Response<EnqueryHistoryResponse> response) {
                if (response.code() == 200) {
                    if (AppUtils.isValidResponse(response.body().API_STATUS)) {
                        enqueryHistoryInterface.enqueryHistoryResponse(response.body());
                    } else {
                        ((BaseActivity)_context).hideProgressDialog();
                        AppUtils.showApiError(_context, response.body().ERROR.MESSAGES);
                    }
                } else {
                    ((BaseActivity)_context).hideProgressDialog();
                    AppUtils.showError(_context, response);
                }
            }

            @Override
            public void onFailure(Call<EnqueryHistoryResponse> call, Throwable t) {
                ((BaseActivity)_context).hideProgressDialog();
                ((BaseActivity)_context).showErrorToast(t.getMessage());
            }
        });
    }

    public void hitLogoutService(String token){
        Call<SuccessResponse> call = ((BaseActivity)_context).jsonService.logoutRequest(token);
        call.enqueue(new Callback<SuccessResponse>() {
            @Override
            public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
                if (response.code() == 200) {
                    if (AppUtils.isValidResponse(response.body().API_STATUS)) {
                        enqueryHistoryInterface.logoutSuccessOps();
                    } else {
                        ((BaseActivity)_context).hideProgressDialog();
                        AppUtils.showApiError(_context, response.body().ERROR.MESSAGES);
                    }
                } else {
                    ((BaseActivity)_context).hideProgressDialog();
                    AppUtils.showError(_context, response);
                }
            }

            @Override
            public void onFailure(Call<SuccessResponse> call, Throwable t) {
                ((BaseActivity)_context).hideProgressDialog();
                ((BaseActivity)_context).showErrorToast(t.getMessage());
            }
        });
    }
}
