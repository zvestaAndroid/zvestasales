package com.zvestasales.presenters.webservices;

import android.content.Context;

import com.zvestasales.global.BaseActivity;
import com.zvestasales.models.data_model.koasModels.request.EnqueryRequest;
import com.zvestasales.models.data_model.koasModels.response.EnqueryResponse;
import com.zvestasales.presenters.interfaces.AddLeadsInterface;
import com.zvestasales.utils.AppUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by test on 10/26/2017.
 */

public class AddLeadsWebservice {

    Context _context;
    AddLeadsInterface addLeadsInterface;

    public AddLeadsWebservice(AddLeadsInterface addLeadsInterface, Context context){
        this.addLeadsInterface = addLeadsInterface;
        this._context = context;
    }

    public void hitAddLeadsService(String token, EnqueryRequest request){
        Call<EnqueryResponse> call = ((BaseActivity)_context).jsonService.addKoasRequest(token, request);
        call.enqueue(new Callback<EnqueryResponse>() {
            @Override
            public void onResponse(Call<EnqueryResponse> call, Response<EnqueryResponse> response) {
                if (response.code() == 200) {
                    if (AppUtils.isValidResponse(response.body().API_STATUS)) {
                        addLeadsInterface.submitAddLeadResponse(response.body());
                    } else {
                        ((BaseActivity)_context).hideProgressDialog();
                        AppUtils.showApiError(_context, response.body().ERROR.MESSAGES);
                    }
                } else {
                    ((BaseActivity)_context).hideProgressDialog();
                    AppUtils.showError(_context, response);
                }
            }

            @Override
            public void onFailure(Call<EnqueryResponse> call, Throwable t) {
                ((BaseActivity)_context).hideProgressDialog();
                ((BaseActivity)_context).showErrorToast(t.getMessage());
            }
        });
    }
}
