package com.zvestasales.presenters.webservices;

import android.content.Context;

import com.zvestasales.global.BaseActivity;
import com.zvestasales.models.data_model.signupModels.request.SignUpModelRequest;
import com.zvestasales.models.data_model.signupModels.response.SignUpResponse;
import com.zvestasales.presenters.interfaces.SignupInterface;
import com.zvestasales.utils.AppUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by test on 10/9/2017.
 */

public class SignupWebservice {

    SignupInterface signupInterface;
    Context _context;

    public SignupWebservice(SignupInterface signupInterface, Context context){
        this.signupInterface = signupInterface;
        this._context = context;
    }

    public void hitSubmitService(SignUpModelRequest request){

        Call<SignUpResponse> call = ((BaseActivity)_context).jsonService.registerRequest(request);
        call.enqueue(new Callback<SignUpResponse>() {
            @Override
            public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                ((BaseActivity)_context).hideProgressDialog();
                if (response.code() == 200) {
                    if (AppUtils.isValidResponse(response.body().API_STATUS)) {
                        signupInterface.submitSignUpResponse(response.body());
                    } else {
                        AppUtils.showApiError(_context, response.body().ERROR.MESSAGES);
                    }
                } else {
                    ((BaseActivity)_context).hideProgressDialog();
                    AppUtils.showError(_context, response);
                }
            }

            @Override
            public void onFailure(Call<SignUpResponse> call, Throwable t) {
                ((BaseActivity)_context).hideProgressDialog();
                ((BaseActivity)_context).showErrorToast(t.getMessage());
            }
        });
    }
}
