package com.zvestasales.presenters.webservices;

import android.content.Context;

import com.zvestasales.global.BaseActivity;
import com.zvestasales.models.data_model.SuccessResponse;
import com.zvestasales.models.data_model.signupModels.request.ForgotPasswordRequest;
import com.zvestasales.models.data_model.signupModels.request.ResendOtpRequest;
import com.zvestasales.presenters.interfaces.ResetPasswordInterface;
import com.zvestasales.utils.AppUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by test on 10/24/2017.
 */

public class ResetPasswordWebservice {

    Context _context;
    ResetPasswordInterface resetPasswordInterface;

    public ResetPasswordWebservice(ResetPasswordInterface resetPasswordInterface, Context context){
        this.resetPasswordInterface = resetPasswordInterface;
        this._context = context;
    }

    public void hitResendOtpService(ResendOtpRequest request){
        Call<SuccessResponse> call = ((BaseActivity)_context).jsonService.resendOtpRequest(request);
        call.enqueue(new Callback<SuccessResponse>() {
            @Override
            public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
                if (response.code() == 200) {
                    if (AppUtils.isValidResponse(response.body().API_STATUS)) {
                        ((BaseActivity)_context).showToast("Please check your mobile number for reset password.");
                    } else {
                        ((BaseActivity)_context).hideProgressDialog();
                        AppUtils.showApiError(_context, response.body().ERROR.MESSAGES);
                    }
                } else {
                    ((BaseActivity)_context).hideProgressDialog();
                    AppUtils.showError(_context, response);
                }
            }

            @Override
            public void onFailure(Call<SuccessResponse> call, Throwable t) {
                ((BaseActivity)_context).hideProgressDialog();
                ((BaseActivity)_context).showErrorToast(t.getMessage());
            }
        });
    }

    public void hitResetPasswordService(ForgotPasswordRequest request){
        Call<SuccessResponse> call = ((BaseActivity)_context).jsonService.forgotPasswordRequest(request);
        call.enqueue(new Callback<SuccessResponse>() {
            @Override
            public void onResponse(Call<SuccessResponse> call, Response<SuccessResponse> response) {
                if (response.code() == 200) {
                    if (AppUtils.isValidResponse(response.body().API_STATUS)) {
                        resetPasswordInterface.submitResetPassword(response.body().API_STATUS);
                    } else {
                        ((BaseActivity)_context).hideProgressDialog();
                        AppUtils.showApiError(_context, response.body().ERROR.MESSAGES);
                    }
                } else {
                    ((BaseActivity)_context).hideProgressDialog();
                    AppUtils.showError(_context, response);
                }
            }

            @Override
            public void onFailure(Call<SuccessResponse> call, Throwable t) {
                ((BaseActivity)_context).hideProgressDialog();
                ((BaseActivity)_context).showErrorToast(t.getMessage());
            }
        });
    }
}
