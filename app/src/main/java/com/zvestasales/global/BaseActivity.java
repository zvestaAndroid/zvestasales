package com.zvestasales.global;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.zvestasales.R;
import com.zvestasales.customviews.NotificationDialogs;
import com.zvestasales.network.JSONService;
import com.zvestasales.utils.loader_util.LoaderView;


public class BaseActivity extends AppCompatActivity {

    public Gson gson;
    public JSONService jsonService;

    public Dialog customLoaderView;
    public LoaderView loaderView;
    private Animation anim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initializeJsonService();
        if (!isNetworkConnected()) {
            new NotificationDialogs().showNetworkAlertDialog(BaseActivity.this, "Network Error", "Check Internet Connection", "Retry", "Cancel");
        }
    }

    /**
     * Initialize gson and jsonService for network calling
     */
    private void initializeJsonService() {
        gson = new Gson();
//        gson = new GsonBuilder()
//                .setLenient()
//                .create();
        jsonService = new JSONService();
    }

    /**
     * @return
     * Check network connectivity
     */
    protected boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    /**
     * @param context
     * @return
     * Show progress dialog
     */
    public boolean showProgressDialog(Context context) {
        if (!isNetworkConnected()) {
            new NotificationDialogs().showNetworkAlertDialog(BaseActivity.this, "Network Error", "Check Internet Connection", "Retry", "Cancel");
            return false;
        }
        customLoaderView = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.toolbar_color));
        }
        customLoaderView.setContentView(R.layout.loader_item);
        LinearLayout llProgressView = (LinearLayout) customLoaderView.findViewById(R.id.llProgressView);
        loaderView = new LoaderView(context);
        llProgressView.addView(loaderView);
        loaderView.setBackgroundColor(Color.TRANSPARENT);

        if (anim == null) {
            createAnimation(llProgressView);
        }
        customLoaderView.show();
        return true;
    }

    /**
     * @param view
     * create progress dialog animation
     */
    protected void createAnimation(LinearLayout view) {
        anim = new RotateAnimation(0, 360,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        anim.setRepeatCount(Animation.INFINITE);
        anim.setRepeatMode(Animation.RESTART);
        anim.setDuration(500L);
        view.startAnimation(anim);
    }

    /**
     * Hide progress dialog
     */
    public void hideProgressDialog() {
        if (customLoaderView != null) {
            customLoaderView.dismiss();
            customLoaderView = null;
        }
        if(anim != null)
            anim = null;
    }

    /**
     * @param stringText
     * Show all message.
     */
    public void showToast(String stringText) {
        Toast.makeText(getBaseContext(), stringText, Toast.LENGTH_SHORT).show();
    }

    /**
     * @param errorString
     * Show error message.
     */
    public void showErrorToast(String errorString) {
        Toast.makeText(getBaseContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    /**
     * @param toolbar
     * @param titleText
     * @param saveText
     * @param res
     * Set toolbar views
     */
    public void setToolbar(Toolbar toolbar, String titleText, String saveText, int res){
        TextView tvToolbarTitle = (TextView) toolbar.findViewById(R.id.tvToolbarTitle);
        TextView tvToolbarSave = (TextView) toolbar.findViewById(R.id.tvToolbarSave);
        ImageView ivNext = (ImageView) toolbar.findViewById(R.id.ivNext);
        if(saveText.equals(""))
            tvToolbarSave.setVisibility(View.GONE);
        if(res > 0){
            ivNext.setImageResource(res);
            ivNext.setVisibility(View.VISIBLE);
        }
        tvToolbarTitle.setText(titleText);
        tvToolbarSave.setText(saveText);
    }

    /**
     * @param toolbar
     * @param isBackEnabled
     * @param navigationIconTintColor
     * Set toolbar default default click event
     */
    public void setToolbar(Toolbar toolbar, boolean isBackEnabled, int navigationIconTintColor) {
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            if (isBackEnabled) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        if (navigationIconTintColor != -1) {
            if (toolbar != null) {
                if (toolbar.getNavigationIcon() != null)
                    toolbar.getNavigationIcon().setTint(ContextCompat.getColor(BaseApplication.getAppContext(), navigationIconTintColor));
            }
        }
    }

    public void setToolbar(Toolbar toolbar, boolean isBackEnabled, String customClick) {
        setSupportActionBar(toolbar);
        if (isBackEnabled) {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            }
        }
    }

    /**
     * @param id
     * @param onClickListener
     * Initialize view onClickListener
     */
    public void setViewOnClick(int id, View.OnClickListener onClickListener) {
        View view = findViewById(id);
        if (view != null)
            view.setOnClickListener(onClickListener);
        else
            Log.e(getClass().getName(), "View with " + id + " id is null");
    }

    /**
     * @param visibility
     * @param ids
     * Set single/multiple view visibility.
     */
    public void setViewVisibility(int visibility, int... ids) {
        for(int id : ids){
            findViewById(id).setVisibility(visibility);
        }
    }

    // This is used for fragment view
    public void setViewVisibility(View mainView, int visibility, int... ids) {
        for (int id : ids) {
            ((View) mainView.findViewById(id)).setVisibility(visibility);
        }
    }

    /**
     * For hiding keyboard.
     */
    public void hideSoftKeyboard() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
}
