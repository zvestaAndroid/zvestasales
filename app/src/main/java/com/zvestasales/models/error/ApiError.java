package com.zvestasales.models.error;

public class ApiError {
    public int statusCode;
    public String error;
    public String message;
}
