package com.zvestasales.models.data_model.signupModels.request;

/**
 * Created by test on 10/23/2017.
 */

public class ResendOtpRequest {
    public String mobile;
    public String source;
}
