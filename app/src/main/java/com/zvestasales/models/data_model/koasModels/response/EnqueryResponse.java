package com.zvestasales.models.data_model.koasModels.response;


import com.zvestasales.models.error.ErrorMessage;

/**
 * Created by test on 10/26/2017.
 */

public class EnqueryResponse {
    public String API_STATUS;
    public EnqueryDetailsModel SUCCESS;
    public ErrorMessage ERROR;
}
