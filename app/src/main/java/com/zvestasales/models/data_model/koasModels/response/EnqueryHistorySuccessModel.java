package com.zvestasales.models.data_model.koasModels.response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by test on 10/30/2017.
 */

public class EnqueryHistorySuccessModel {
    public List<EnqueryDetailsModel> enquiries;

    public EnqueryHistorySuccessModel(){
        enquiries = new ArrayList<>();
    }
}
