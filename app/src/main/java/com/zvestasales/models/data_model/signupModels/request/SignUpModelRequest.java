package com.zvestasales.models.data_model.signupModels.request;

/**
 * Created by test on 9/26/2017.
 */

public class SignUpModelRequest {
    public String first_name;
    public String last_name;
    public String email;
    public String mobile;
    public String password;
    public String user_type_id;
    public String source_type;
}
