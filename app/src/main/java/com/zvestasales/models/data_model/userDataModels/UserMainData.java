package com.zvestasales.models.data_model.userDataModels;

public class UserMainData {

    public String userId;
    public String fName;
    public String lName;
    public String email;
    public String mobile;
    public String userTypeId;
    public String sourceType;
    public String userProfPic;
    public String token;
}
