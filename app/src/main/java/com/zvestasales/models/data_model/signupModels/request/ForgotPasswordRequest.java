package com.zvestasales.models.data_model.signupModels.request;

/**
 * Created by test on 10/24/2017.
 */

public class ForgotPasswordRequest {
    public String mobile;
    public String otp;
    public String password;
}
