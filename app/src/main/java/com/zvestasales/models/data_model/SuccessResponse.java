package com.zvestasales.models.data_model;


import com.zvestasales.models.error.ErrorMessage;

/**
 * Created by test on 9/26/2017.
 */

public class SuccessResponse {
    public String API_STATUS;
    public ErrorMessage ERROR;
}
