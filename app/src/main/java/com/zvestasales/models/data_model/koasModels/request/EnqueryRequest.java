package com.zvestasales.models.data_model.koasModels.request;

/**
 * Created by test on 10/26/2017.
 */

public class EnqueryRequest {
    public String first_name;
    public String last_name;
    public String email;
    public String mobile;
    public String budget;
    public String comment;
    public String best_time;
    public String occupation;
    public String city;
    public String gender;
    public String marital_status;
}
