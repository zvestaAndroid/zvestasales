package com.zvestasales.models.data_model.koasModels.response;


import com.zvestasales.models.error.ErrorMessage;

/**
 * Created by test on 10/30/2017.
 */

public class EnqueryHistoryResponse {
    public String API_STATUS;
    public EnqueryHistorySuccessModel SUCCESS;
    public ErrorMessage ERROR;
}
