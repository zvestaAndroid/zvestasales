package com.zvestasales.models.data_model.signupModels.response;


import com.zvestasales.models.error.ErrorMessage;

/**
 * Created by test on 9/26/2017.
 */

public class SignUpResponse {
    public String API_STATUS;
    public SignUpSuccessModel SUCCESS;
    public ErrorMessage ERROR;
}
