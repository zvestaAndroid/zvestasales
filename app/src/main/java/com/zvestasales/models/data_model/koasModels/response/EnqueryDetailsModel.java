package com.zvestasales.models.data_model.koasModels.response;

/**
 * Created by test on 10/26/2017.
 */

public class EnqueryDetailsModel {
    public String id;
    public String name;
    public String email;
    public String mobile;
    public String budget;
    public String comment;
    public String enquiry_type;
    public String status;
    public String source;
    public String created_at;
    public String updated_at;
    public String occupation;
    public String best_time;
    public String city;
}