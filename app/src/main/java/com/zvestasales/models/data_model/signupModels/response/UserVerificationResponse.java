package com.zvestasales.models.data_model.signupModels.response;


import com.zvestasales.models.error.ErrorMessage;

/**
 * Created by test on 10/13/2017.
 */

public class UserVerificationResponse {
    public String API_STATUS;
    public VerificationSuccessModel SUCCESS;
    public ErrorMessage ERROR;
}
