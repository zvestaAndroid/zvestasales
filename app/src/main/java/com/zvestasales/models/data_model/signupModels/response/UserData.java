package com.zvestasales.models.data_model.signupModels.response;

/**
 * Created by test on 10/6/2017.
 */

public class UserData {
    public String first_name;
    public String last_name;
    public String email;
    public String mobile;
    public String user_type_id;
    public String user_id;
    public String is_verified;
}
