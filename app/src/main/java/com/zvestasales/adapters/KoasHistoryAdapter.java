package com.zvestasales.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zvestasales.R;
import com.zvestasales.activities.koas_flow.KoasHistoryActivity;
import com.zvestasales.models.data_model.koasModels.response.EnqueryDetailsModel;
import com.zvestasales.models.data_model.userDataModels.UserMainData;
import com.zvestasales.utils.global_data_utils.CurrentStateUtil;

import java.util.List;

/**
 * Created by test on 10/28/2017.
 */

public class KoasHistoryAdapter extends RecyclerView.Adapter<KoasHistoryAdapter.ViewHolder>{

    List<EnqueryDetailsModel> historyList;
    Context _context;
    UserMainData userMainData;

    public KoasHistoryAdapter(Context context, List<EnqueryDetailsModel> historyList){
        this._context = context;
        this.historyList = historyList;
        userMainData = CurrentStateUtil.getCurrentUserMainData();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View mainView;
        TextView tvSellerName, tvSellerMobile, tvSlno;

        public ViewHolder(View v){
            super(v);
            mainView = v;
            initializeViewIds(v);
        }

        private void initializeViewIds(View view){
            tvSellerName = (TextView) view.findViewById(R.id.tvSellerName);
            tvSlno = (TextView) view.findViewById(R.id.tvSlno);
            tvSellerMobile = (TextView) view.findViewById(R.id.tvSellerMobile);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView;
        ViewHolder vh;
        rowView = LayoutInflater.from(_context).inflate(R.layout.history_item, parent, false);
        vh = new ViewHolder(rowView);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        EnqueryDetailsModel item = historyList.get(position);
        performOps(holder, item, position);
    }

    private void performOps(ViewHolder holder, EnqueryDetailsModel item, final int position){
        holder.tvSellerName.setText(item.name);
        holder.tvSlno.setText(""+ (position+1));
        holder.tvSellerMobile.setText(item.mobile);

        holder.mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((KoasHistoryActivity)_context).showDialogForKioskDetails(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }
}
