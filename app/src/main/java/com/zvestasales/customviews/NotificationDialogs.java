package com.zvestasales.customviews;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.zvestasales.R;

public class NotificationDialogs {

    public NotificationCallbacks notificationCallbacks;

    public NotificationDialogs() {
    }

    public NotificationDialogs(NotificationCallbacks notificationCallbacks) {
        this.notificationCallbacks = notificationCallbacks;
    }

    public void showNetworkAlertDialog(final Context context, String title,
                                       String message, String posBtnValue, String negBtnValue) {
        LayoutInflater li = LayoutInflater.from(context);
        View promtView = li.inflate(R.layout.network_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setView(promtView);
        TextView titleView = (TextView) promtView.findViewById(R.id.title);
        titleView.setText(title);
        if(posBtnValue == null || posBtnValue.equals(""))
            posBtnValue = context.getResources().getString(R.string.ok_text);
        builder.setPositiveButton(posBtnValue, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((Activity)context).finish();
                context.startActivity(((Activity) context).getIntent());
            }
        });
        if(negBtnValue == null || negBtnValue.equals(""))
            negBtnValue = context.getResources().getString(R.string.cancel_text);
        builder.setNegativeButton(negBtnValue, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((Activity)context).finish();
            }
        });
        builder.show();
    }

    public void showDefaultAlertDialog(Context context, String title,
                                       String message, String okText, String cancelText, final String notificationType) {
        if(okText == null || okText.equals(""))
            okText = context.getResources().getString(R.string.yes_text);
        if(cancelText == null || cancelText.equals(""))
            cancelText = context.getResources().getString(R.string.no_text);
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(okText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with close
                        notificationCallbacks.onYesPressed(notificationType);
                    }
                })
                .setNegativeButton(cancelText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        notificationCallbacks.onNoPressed(notificationType);
                    }
                })
                .show();
    }

    public interface NotificationCallbacks {
        public void onYesPressed(String type);
        public void onNoPressed(String type);
    }
}
