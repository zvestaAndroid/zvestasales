package com.zvestasales.network;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

// interceptor to log request and response using okhttp
public class LoggingInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        long t1 = System.nanoTime();
        Log.i(getClass().getName(), "Sending request " + request.url() + " on " + chain.connection() +
                request.headers());

        Response response = chain.proceed(request);

        long t2 = System.nanoTime();
        Log.i(getClass().getName(), "Received response for " + response.request().url() + " in " + (t2 - t1) +
                "ms " + response.headers());
        return response;
    }
}