package com.zvestasales.network;

import com.google.gson.Gson;
import com.zvestasales.models.data_model.SuccessResponse;
import com.zvestasales.models.data_model.koasModels.request.EnqueryRequest;
import com.zvestasales.models.data_model.koasModels.response.EnqueryHistoryResponse;
import com.zvestasales.models.data_model.koasModels.response.EnqueryResponse;
import com.zvestasales.models.data_model.signupModels.request.ForgotPassOtpRequest;
import com.zvestasales.models.data_model.signupModels.request.ForgotPasswordRequest;
import com.zvestasales.models.data_model.signupModels.request.LoginRequest;
import com.zvestasales.models.data_model.signupModels.request.ResendOtpRequest;
import com.zvestasales.models.data_model.signupModels.request.SignUpModelRequest;
import com.zvestasales.models.data_model.signupModels.request.UserVerificationRequest;
import com.zvestasales.models.data_model.signupModels.response.SignUpResponse;
import com.zvestasales.models.data_model.signupModels.response.UserVerificationResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

// manages all the urls
public class JSONService extends NetworkBaseService {
    public static Retrofit restAdapter;
    public RetroApi retroApi;

    public JSONService() {
        restAdapter = restAdapterBuilder.addConverterFactory(GsonConverterFactory.create()).build();
        retroApi = restAdapter.create(RetroApi.class);
    }

    public JSONService(Gson gson) {
        restAdapter = restAdapterBuilder.addConverterFactory(GsonConverterFactory.create(gson)).build();
        retroApi = restAdapter.create(RetroApi.class);
    }

    /*********Sign up flow********/
    public Call<SignUpResponse> registerRequest(SignUpModelRequest request){
        return retroApi.registerRequest(request);
    }

    public Call<SignUpResponse> loginRequest(LoginRequest request){
        return retroApi.loginRequest(request);
    }

    public Call<SuccessResponse> logoutRequest(String token){
        return retroApi.logoutRequest(token);
    }

    public Call<UserVerificationResponse> userVerificationRequest(UserVerificationRequest request){
        return retroApi.userVerificationRequest(request);
    }

    public Call<SuccessResponse> resendOtpRequest(ResendOtpRequest request){
        return retroApi.resendOtpRequest(request);
    }

    public Call<SuccessResponse> forgotPassOtpRequest(ForgotPassOtpRequest request){
        return retroApi.forgotPassOtpRequest(request);
    }

    public Call<SuccessResponse> forgotPasswordRequest(ForgotPasswordRequest request){
        return retroApi.forgotPasswordRequest(request);
    }

    /******Koas screen*****/
    public Call<EnqueryResponse> addKoasRequest(String token, EnqueryRequest request){
        return retroApi.addKoasRequest(token, request);
    }

    public Call<EnqueryHistoryResponse> koasHistoryRequest(String token, int pageNo){
        return retroApi.koasHistoryRequest(token, pageNo);
    }

    interface RetroApi {

        /******Sign up flow******/
        @POST("register")
        Call<SignUpResponse> registerRequest(@Body SignUpModelRequest request);

        @POST("login")
        Call<SignUpResponse> loginRequest(@Body LoginRequest request);

        @POST("logout")
        Call<SuccessResponse> logoutRequest(@Header("Authorization") String token);

        @POST("userVerification")
        Call<UserVerificationResponse> userVerificationRequest(@Body UserVerificationRequest request);

        @POST("resendOtp")
        Call<SuccessResponse> resendOtpRequest(@Body ResendOtpRequest request);

        @POST("forgetPassword")
        Call<SuccessResponse> forgotPassOtpRequest(@Body ForgotPassOtpRequest request);

        @POST("changeForgetPassword")
        Call<SuccessResponse> forgotPasswordRequest(@Body ForgotPasswordRequest request);

        /******Koas screen*****/
        @POST("kiosk")
        Call<EnqueryResponse> addKoasRequest(@Header("Authorization") String token,
                                             @Body EnqueryRequest request);

        @POST("getEnquiry/page/{pageNo}")
        Call<EnqueryHistoryResponse> koasHistoryRequest(@Header("Authorization") String token,
                                                        @Path("pageNo") int pageNo);
    }
}
