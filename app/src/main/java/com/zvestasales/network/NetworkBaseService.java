package com.zvestasales.network;

import com.zvestasales.utils.global_data_utils.GlobalDataUtils;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public class NetworkBaseService {

    String API = GlobalDataUtils.BASE_API;

    // used for log and other operations
    OkHttpClient client = new OkHttpClient.Builder()
            .addInterceptor(new LoggingInterceptor())
            .build();

    Retrofit.Builder restAdapterBuilder = new Retrofit.Builder().baseUrl(API).client(client);
}
